package com.meenu.foodtrucks;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by meenu on 15-06-02.
 */
public class NetworkConnection extends AsyncTask<String, Void, String> {

    private ConnectionResult resultCallback;
    private Context context;
    private RequestTypes type;

    public NetworkConnection(ConnectionResult connectionResult, Context context, RequestTypes type) {
        this.resultCallback = connectionResult;
        this.context = context;
        this.type = type;
    }

    @Override
    protected String doInBackground(String... params) {
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet getRequest = new HttpGet(params[0]);
        String json = null;
        Bundle data = new Bundle();
        try {
            HttpResponse response = httpClient.execute(getRequest);
            return EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String response) {
        Bundle data = new Bundle();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (this.type == RequestTypes.EVENT_LIST) {
                if (jsonObject.has("data")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    String[] eventName = new String[jsonArray.length()];
                    String[] eventStart = new String[jsonArray.length()];
                    for (int i = 0; i < jsonArray.length(); i++) {
                        eventName[i] = jsonArray.getJSONObject(i).getString("name");
                        eventStart[i] = jsonArray.getJSONObject(i).getString("start_time");
                    }
                    data.putStringArray("event_list_names", eventName);
                    data.putStringArray("event_list_dates", eventStart);
                    data.putString("load_more_url", jsonObject.getJSONObject("paging").getString("next"));
                    resultCallback.connectionSuccessful(data);
                } else {
                    if (jsonObject.has("error") && jsonObject.getJSONObject("error").get("type").equals("OAuthException")) {
                        data.putString("error", "Please get a new access token and place it in strings.xml and rerun the app");
                        resultCallback.connectionFailure(data);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
