package com.meenu.foodtrucks;

import android.os.Bundle;

public interface ConnectionResult {
    void connectionSuccessful(Bundle data);
    void connectionFailure(Bundle data);
}
