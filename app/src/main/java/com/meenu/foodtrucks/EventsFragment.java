package com.meenu.foodtrucks;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EventsFragment extends Fragment implements ConnectionResult {

    EventListAdapter eventListAdapter;
    ListView eventList;
    ProgressBar progressBar;
    Button loadMoreBtn;
    TextView errorLabel;

    String loadMoreUrl = "";
    List<String> historicEventNames = new ArrayList<>();
    List<String> historicEventDates = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View rootView = inflater.inflate(R.layout.fragment_events, container, false);
        eventList = (ListView) rootView.findViewById(R.id.eventList);
        progressBar = (ProgressBar) rootView.findViewById(R.id.eventListProgress);
        loadMoreBtn = (Button) rootView.findViewById(R.id.loadMore);
        errorLabel = (TextView) rootView.findViewById(R.id.errorLabel);
        loadMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!loadMoreUrl.isEmpty()) {
                    loadMoreBtn.setVisibility(View.GONE);
                    eventList.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    new NetworkConnection(EventsFragment.this, getActivity(), RequestTypes.EVENT_LIST).execute(loadMoreUrl);
                }
            }
        });

        new NetworkConnection(EventsFragment.this, getActivity(), RequestTypes.EVENT_LIST).execute("https://graph.facebook.com/v2.3/OffTheGridSF/events?access_token="
                + getActivity().getResources().getString(R.string.page_access_token));
        progressBar.setVisibility(View.VISIBLE);
        loadMoreBtn.setVisibility(View.GONE);
        return rootView;
    }

    @Override
    public void connectionSuccessful(Bundle data) {
        errorLabel.setVisibility(View.GONE);
        String[] eventNames = data.getStringArray("event_list_names");
        String[] eventDates = data.getStringArray("event_list_dates");
        historicEventNames.addAll(Arrays.asList(eventNames));
        historicEventDates.addAll(Arrays.asList(eventDates));
        loadMoreUrl = data.getString("load_more_url");
        eventListAdapter = new EventListAdapter(getActivity(), R.layout.events_list_item, historicEventNames, historicEventDates);
        eventList.setAdapter(eventListAdapter);
        progressBar.setVisibility(View.GONE);
        loadMoreBtn.setVisibility(View.VISIBLE);
        eventList.setVisibility(View.VISIBLE);

    }

    @Override
    public void connectionFailure(Bundle data) {
        eventList.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        errorLabel.setText(data.getString("error"));
        errorLabel.setVisibility(View.VISIBLE);
    }

    private class EventListAdapter extends ArrayAdapter {
        List<String> eventNames;
        List<String> eventDates;
        Context context;

        public EventListAdapter(Context context, int resourceId, List<String> eventNames, List<String> eventDates) {
            super(context, resourceId, eventNames);
            this.eventNames = eventNames;
            this.eventDates = eventDates;
            this.context = context;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.events_list_item, parent, false);
            TextView nameView = (TextView) rowView.findViewById(R.id.eventName);
            TextView dateView = (TextView) rowView.findViewById(R.id.eventDate);
            nameView.setText(eventNames.get(position));
            dateView.setText(eventDates.get(position));
            return rowView;
        }
    }
}
